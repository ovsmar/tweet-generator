from PIL import Image, ImageDraw, ImageFont,ImageOps
import textwrap
from pathlib import Path
import os.path



def stiker_main(astr,StikerID):
    # astr = '''Христос уже имеет свой капитал.'''
    astr2 = '''@TheMashina404'''

    para = textwrap.wrap(astr, width=23)
    para2 = textwrap.wrap(astr2, width=15)

    MAX_W, MAX_H = 502, 502
    #creer image
    im = Image.new('RGB', (MAX_W, MAX_H), ("white"))
    draw = ImageDraw.Draw(im)
    #les fonts
    font = ImageFont.truetype("Pillow/Tests/fonts/FreeMono.ttf", 35)
    font2 = ImageFont.truetype("Pillow/Tests/fonts/FreeMono.ttf", 22)

    #text de tweet
    current_h, pad = 200, 10
    for line in para:
        w, h = draw.textsize(line, font=font)
        draw.text(((MAX_W - w) / 2, current_h), line, font=font,fill=(0,0,0))
        current_h += h + pad

    #text de channel
    current_h2, pad2 = 460, 10
    for line in para2:
        w, h = draw.textsize(line, font=font2)
        draw.text(((MAX_W - w) / 2, current_h2), line, font=font2,fill=(0,0,0))
        current_h2 += h + pad2

    
    #enregistrer image
    im.save(f"pack/{StikerID}.png")

    if os.path.isfile(f"pack/{StikerID}.png"):
    #ajouter border noir sur l'image et enregistrer 
        ImageOps.expand(Image.open(f"pack/{StikerID}.png"),border=5,fill='black').save(f"pack2/{StikerID}.png")
        #supprimer tout dans le /pack
        [f.unlink() for f in Path("pack").glob("*") if f.is_file()]
    else: 
        print("non")

