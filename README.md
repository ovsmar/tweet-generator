# tweet-generator

Le projet Tweet Generator est une application qui a été développée avec les technologies Python, Flask, SQLite3, HTML, CSS, Bootstrap ,JavaScript, Tweepy , API Telegram et Pillow. Elle permet de générer des tweets ou des postes textuels pour Twitter et Telegram en utilisant un algorithme appelé chaîne de Markov, qui est basé sur l'analyse statistique de grandes quantités de texte. L'application utilise également la bibliothèque Tweepy pour se connecter à l'API Twitter et publier les tweets générés sur le compte de l'utilisateur, ainsi que l'API Telegram pour envoyer les postes générés sur un canal Telegram. Elle permet également de générer des autocollants à partir des tweets ou des postes générés pour Telegram, en utilisant la bibliothèque Pillow pour traiter et manipuler les images utilisées dans ces autocollants.

L'application utilise une base de données SQLite3 pour stocker les tweets générés et offre des fonctionnalités de gestion de tweets ou de postes, telles que la génération, la suppression, la publication, la modification, la publication aléatoire et l'archivage de tweets ou de messages, avec la possibilité de lancer un planificateur en arrière-plan pour publier les tweets ou les messages à intervalles réguliers sur Twitter et Telegram.

![tweet-generator](https://ovsepsimonian.go.yo.fr/img/LES-TWEETS-GENERATOR.gif)


