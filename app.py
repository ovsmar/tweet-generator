import sqlite3
import markovify
from time import sleep
from flask import Flask, render_template, request, flash, redirect, url_for
from apscheduler.schedulers.background import BackgroundScheduler
from decouple import config
import atexit
import os.path

import twitter
import sticker
import telegram


app = Flask(__name__)
app.config['SECRET_KEY'] = config('KEY')

#redirect
@app.route('/')
def redirection():
    return redirect('tweets')

#connection BDD
def get_db_connection():
    connection = sqlite3.connect('db/database.db')
    connection.row_factory = sqlite3.Row
    connection.set_trace_callback(print)

    return connection


#markovify + insert into db
def markov():
    print("DONE")
    connection = get_db_connection()
    text = open('db/text2.txt', encoding='utf8').read()

    text_model = markovify.Text(text)
    sentence = text_model.make_sentence(max_chars=280)

    charactersofsentence = len(sentence)

    data = connection.execute('INSERT INTO tweets (tweet,characters) VALUES (?,?)', (sentence,charactersofsentence,))
    connection.commit()
    connection.close()
    


#lancement de la génération des tweets via button
@app.route('/genTweets', methods=['GET', 'POST'])
def genTweets(x=None, y=None):
        global sched
        sched = BackgroundScheduler()
        sched.add_job(markov, 'interval', seconds=1,id='my_job_id')
        app.logger.info("Adding job")
        sched.start()
        while True:
            sleep(1)
            


#stop génération
@app.route('/genTweetsSTOP',methods=['GET', 'POST'])
def genTweetsSTOP(x=None, y=None):
    global sched
    if request.method == 'POST':
        sched.shutdown(wait=False)
        flash('STOP')
        return redirect(url_for('tweets'))


#récupérer des tweets + affichage
@app.route('/tweets')
def tweets():
    connection = get_db_connection()
    db_tweets = connection.execute('SELECT id, tweet, characters, created FROM tweets').fetchall()
    connection.close()

    tweets = []
    for tweet in db_tweets:
        tweet = dict(tweet)
        tweets.append(tweet)

    return render_template('tweets.html', tweets=tweets)       


#Pour suprrimer les tweets
@app.route('/delete', methods=['POST'])
def delete_tweets():
    connection = get_db_connection()

    connection.execute('DELETE FROM tweets WHERE id=?', [request.form['entry_id']])
    connection.commit()
    flash('Supprimé avec succès')
    return redirect(url_for('tweets'))

 
#Pour archive les tweets
@app.route('/archive', methods=['POST'])
def archive_tweets():
    connection = get_db_connection()
    exist = connection.execute("select tweetAR from archives where tweetAR = ?", (request.form['entry_tweet'],)).fetchone()
    if exist is None:
        connection.execute('INSERT INTO archives (tweetAR,charactersAR,createdAR) VALUES (?,?,?)', (request.form['entry_tweet'],request.form['entry_characters'],request.form['entry_created']))
        connection.execute('DELETE FROM tweets WHERE id=?', [request.form['entry_id']])
        flash('archive et supprimmer')
    else:
        flash('deja exist')
        return redirect(url_for('tweets'))
    
    connection.commit()
    
    return redirect(url_for('tweets'))




#Pour afficher les archives
@app.route('/archives')
def archives():
    connection = get_db_connection()
    db_urls = connection.execute('SELECT id, tweetAR, charactersAR, createdAR FROM archives').fetchall()
    connection.close()

    urls = []
    for url in db_urls:
        url = dict(url)
        urls.append(url)

    return render_template('archives.html', urls=urls)


#Pour suprrimer les archives
@app.route('/delete_archives', methods=['POST'])
def delete_archives():
    connection = get_db_connection()

    connection.execute('DELETE FROM archives WHERE id=?', [request.form['ArchiveEentry_id']])
    connection.commit()
    flash('Supprimé avec succès')
    return redirect(url_for('archives'))


#Pour twitter (la route Archive)
@app.route('/twitterArchive', methods=['POST'])
def twitterArchive():
    connection = get_db_connection()

    post = request.form['entry_tweetAR']
    twitter.tweet(post)  
      
    connection.execute('INSERT INTO posts (post,charactersPost) VALUES (?,?)', (request.form['entry_tweetAR'],request.form['entry_charactersPostA']))
    connection.execute('DELETE FROM archives WHERE id=?', [request.form['entry_idPostA']])
    flash('tweeter avec succès')
    
    connection.commit()
    connection.close()
    return redirect(url_for('posts'))
    
    


#Pour twitter (la route Tweets)
@app.route('/twitterTweets', methods=['POST'])
def twitterTweets():
    connection = get_db_connection()

    post = request.form['entry_tweetT']
    twitter.tweet(post)

    connection.execute('INSERT INTO posts (post,charactersPost) VALUES (?,?)', (request.form['entry_tweetT'],request.form['entry_charactersPostT']))
    connection.execute('DELETE FROM tweets WHERE id=?', [request.form['entry_idPostT']])
    flash('tweeter avec succès')

    connection.commit()
    connection.close()
    return redirect(url_for('posts'))


#Pour afficher les posts
@app.route('/posts')
def posts():
    connection = get_db_connection()
    db_posts = connection.execute('SELECT id, post, charactersPost, createdPost FROM posts').fetchall()
    connection.close()

    posts = []
    for post in db_posts:
        post = dict(post)
        posts.append(post)

    return render_template('posts.html', posts=posts)


    

#random post tweet
@app.route('/randomPost', methods=['GET', 'POST'])
def randomPost():
    connection = get_db_connection()
    randomdb = connection.execute('SELECT * from archives order by RANDOM() LIMIT 1').fetchall()
    
    randoms = []
    for random in randomdb:
        random = dict(random)
        randoms.append(random)
        print(randoms)
        print(random['id'])
        print(random['tweetAR'])

        existRandom = connection.execute("select post from posts where post = ?", (random['tweetAR'],)).fetchone()
        if existRandom is None:
            if request.form.get('randomPostTwitter'):
                postR = random['tweetAR']
                twitter.tweet(postR)
            else: 
                if request.form.get('randomPostTelegram'):
                   postT = random['tweetAR']
                   telegram.send_to_telegram(postT)  
                
            connection.execute('INSERT INTO posts (post,charactersPost) VALUES (?,?)', (random['tweetAR'],random['charactersAR']))
            connection.execute('DELETE FROM archives WHERE id=?', [random['id']])
            connection.commit()

            flash('poster avec succès')
            return redirect(url_for('posts'))
        else:
            flash('deja exist , réessayer ')
            return redirect(url_for('archives'))

def test():
    print("hey")
    

#auto posting
@app.route('/autoPost', methods=['GET', 'POST'])
def autoPost():
     with app.app_context():
        global Scheduler
        Scheduler = BackgroundScheduler()
        Scheduler.add_job(randomPost, 'cron', hour=config('hour'), minute =config('minute'), day_of_week="mon,tue,wed,thu,fri,sat,sun")
        app.logger.info("Adding job")
        Scheduler.start()
        # atexit.register(lambda: Scheduler.shutdown())
        # with app.app_context():
        #      autoPost()
            
        return redirect(url_for('posts'))
       

# stop autoPost
@app.route('/autoPostStop',methods=['GET', 'POST'])
def autoPostStop():
    global Scheduler
    if request.method == 'POST':
        Scheduler.shutdown(wait=False)
        flash('STOP')
        return redirect(url_for('tweets'))



#Pour modifer les archives
@app.route('/update_archives', methods=['POST'])
def update_archives():
    connection = get_db_connection()

    connection.execute('UPDATE archives SET tweetAR = ? WHERE  id = ?', [request.form['updateAR'],request.form['updateIdAR']])
    connection.commit()
    flash('updateé avec succès')
    return redirect(('archives' + "#" + request.form['updateIdAR'] ))


#Pour creer stiker
@app.route('/createsStiker', methods=['POST'])
def createsStiker():

    StikerID = request.form['entry_StikerID']
    astr = request.form['entry_StikerPost']

    if os.path.isfile(f"pack2/{StikerID}.png"):

        flash('deja exist dans le dossier /pack2 ')  
    else:
        sticker.stiker_main(astr,StikerID)

        flash('creé avec succès et sauvegardeé dans le dossider /pack')  
        
    return redirect(url_for('posts'))


#Pour suprrimer les posts 
@app.route('/delete_posts', methods=['POST'])
def delete_posts():
    connection = get_db_connection()

    connection.execute('DELETE FROM posts WHERE id=?', [request.form['deletePost_id']])
    connection.commit()
    flash('Supprimé avec succès')
    return redirect(url_for('posts'))


#Pour poster sur telegram channel (la route Archive)
@app.route('/telegramArchive', methods=['POST'])
def telegramArchive():
    connection = get_db_connection()

    post = request.form['entry_TGAR']
    telegram.send_to_telegram(post)  
      
    connection.execute('INSERT INTO posts (post,charactersPost) VALUES (?,?)', (request.form['entry_TGAR'],request.form['entry_charactersPostTG']))
    connection.execute('DELETE FROM archives WHERE id=?', [request.form['entry_idPostTG']])
    flash('telegramé avec succès')
    
    connection.commit()
    connection.close()
    return redirect(url_for('posts'))


#Pour poster sur telegram channel (la route Tweets)
@app.route('/telegramTweets', methods=['POST'])
def telegramTweets():
    connection = get_db_connection()

    post = request.form['entry_TGT']
    telegram.send_to_telegram(post)  

    connection.execute('INSERT INTO posts (post,charactersPost) VALUES (?,?)', (request.form['entry_TGT'],request.form['entry_charactersPostTGT']))
    connection.execute('DELETE FROM tweets WHERE id=?', [request.form['entry_idPostTGT']])
    flash('telegramé avec succès')

    connection.commit()
    connection.close()
    return redirect(url_for('posts'))