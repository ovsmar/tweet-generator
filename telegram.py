import requests
from decouple import config

def send_to_telegram(message):

    apiToken = config('apiToken')
    chatID = config('chatID')
    apiURL = f'https://api.telegram.org/bot{apiToken}/sendMessage'

    try:
        response = requests.post(apiURL, json={'chat_id': chatID, 'text': message})
        print(response.text)
    except Exception as e:
        print(e)

