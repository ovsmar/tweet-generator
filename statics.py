import tweepy
from decouple import config

bearer_token = config('bearer_token')

client = tweepy.Client(bearer_token)


user_id = config('user_id')

response = client.get_users_followers(user_id)
print(response)

for user in response.data:
    print(user.username)

response = client.get_users_followers(user_id, max_results=1000)
